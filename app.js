$(function() {
  if (window.PIE) {
    $('.rounded').each(function() {
      PIE.attach(this);
    });
  }

  // menu de droite fixe au scroll
  $(".column-right-outer aside").followTo(250, "aside-fixed");
  // ajout de la date
  $("header").append("<span id='today'>" + moment().format("dddd DD MMMM YYYY") + "</span>");

  initMouse();
  draw('white', 'draw', 3);	
  generateBG(100);
  
  // wait until search gadget load
  var scan = self.setInterval(function() {
    if ($('input.gsc-search-button').length > 0) {
      // gadget loaded
      // stop scan
      clearInterval(scan);
      // apply style
      stylishSearchBox();
    }
  }, 100);
  
  // Declare parallax on layers
  //$('.parallax-layer').parallax({}, {xparallax: '0px'}, {xparallax: '10px'}, {xparallax: '50px'}, {xparallax: '200px'});
  
  //$("#img_accueil").imageLens({ lensSize: 200 });
});

var windw = this;
$.fn.followTo = function (pos, className) {
  var $this = this,
    $window = $(windw);
    
  $window.scroll(function(e){
    if ($window.scrollTop() > pos) {
      $this.addClass(className);
    } else {
      $this.removeClass(className);
    }
  });
};

var canvasMouse, contextMouse;

var started = false;
var x, y;

function initMouse() {

  // Get the drawing canvas
	canvasMouse = document.getElementById('blackboard');
	if (!canvasMouse) {
    return;
  }
	contextMouse = canvasMouse.getContext('2d');
    
    contextMouse.strokeStyle = '#000';
    contextMouse.lineWidth = '1';
    
	// Add some event listeners so we can figure out what's happening
	// and run a few functions when they are executed.
	canvasMouse.addEventListener('mousemove', mousemovement, false);
	canvasMouse.addEventListener('mousedown', mouseclick, false);
	canvasMouse.addEventListener('mouseup', mouseunclick, false);

}

function mouseclick() { 
  if (!contextMouse) {
    return;
  }
	// When the mouse is clicked. Change started to true and move
	// the initial position to the position of the mouse
	contextMouse.beginPath();
	contextMouse.moveTo(x, y);
	started = true;
	
}

// For getting the mouse position, basically. This gets the position
// of the canvas element, so we can use it to calculate the mouse 
// position on the canvas
function getOffset(e) {
    var cx = 0;
    var cy = 0;
    
    while(e && !isNaN(e.offsetLeft) && !isNaN(e.offsetTop)) {
        cx += e.offsetLeft - e.scrollLeft;
        cy += e.offsetTop - e.scrollTop;
        e = e.offsetParent;
    }
    return { top: cy, left: cx };
}

function mousemovement(e) {
	
	// Get mouse position
	if(e.offsetX || e.offsetY) {
		x = e.pageX - getOffset(document.getElementById('blackboard')).left - window.pageXOffset;
		y = e.pageY - getOffset(document.getElementById('blackboard')).top - window.pageYOffset;
	}
	else if(e.layerX || e.layerY) {
		x = (e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft) - getOffset(document.getElementById('blackboard')).left - window.pageXOffset;
		y = (e.clientY + document.body.scrollTop + document.documentElement.scrollTop) - getOffset(document.getElementById('blackboard')).top;
	}	
		
	// If started is true, then draw a line
	if(started) {
		contextMouse.lineTo(x, y);
		contextMouse.stroke();
	}
}

function mouseunclick() {
	// Change started to false when the user unclicks the mouse
	if(started) {
		started = false;	
	}
}

// Change size
function changeSize(s) {
  if (!contextMouse) {
    return;
  }
	contextMouse.lineWidth = s;
}

// Change color
function changeColor(c) { 
  if (!contextMouse) {
    return;
  }
	contextMouse.strokeStyle = c;
}

function draw(color, tool, size) {
  changeColor(color);
  $("#blackboard").attr("class", tool);
  changeSize(size);
}

function generateBG(numberOfBGElement) {
  var htmlElement = ".content";

  var images = new Array();
  images.push("https://lh5.googleusercontent.com/-Kosa999_NMI/UL_B_Rms9rI/AAAAAAAAABk/SLsEhXz4gPU/main%252520bleu%252520c.png");
  images.push("https://lh5.googleusercontent.com/-4BCl6tS70Vc/UL_B_wY6XmI/AAAAAAAAABo/0silCLlmx8c/main%252520bleu%252520f.png");
  images.push("https://lh3.googleusercontent.com/-9sIGzlT0fAA/UL_CAV8OSKI/AAAAAAAAAB8/3DW-LyX_uRI/main%252520jaune.png");
  images.push("https://lh4.googleusercontent.com/-x7bcYo_lZLM/UL_CAjiKH5I/AAAAAAAAACI/cYKmMDpaqAQ/main%252520orange.png");
  images.push("https://lh5.googleusercontent.com/-qpDghX3Nk3A/UL_CAwUeyQI/AAAAAAAAACU/z18sTlXzWSQ/main%252520rose.png");
  images.push("https://lh4.googleusercontent.com/-V5mA8crnezc/UL_CBf7TXVI/AAAAAAAAACM/1YRKte1bfRA/main%252520rouge.png");
  images.push("https://lh5.googleusercontent.com/-t_-Zzien2hQ/UL_CBzFudKI/AAAAAAAAACY/qxTU2hV5Ag0/main%252520verte.png");
  images.push("https://lh4.googleusercontent.com/-44tQDPOaxDQ/UL_CCqgy3YI/AAAAAAAAACk/ziwYQxPEzZQ/main%252520violet.png");
  
  var params = { minLeft: 10,  maxLeft: $(htmlElement).width(), minTop: 10, maxTop: $(htmlElement).height() };
  
  var urls = new Array();
  var positions = new Array();
  
  // statics images
  urls.push("url('https://lh5.googleusercontent.com/-we2YbFwnV-Y/UMEv3RtC7FI/AAAAAAAAAEI/OxYl_zhbE_U/taches_de_peinture_couleurs_splash.png')");
  positions.push("20px 130px");
  
  //urls.push("url('https://lh5.googleusercontent.com/-00BUlmSXOd8/UMEv21XEZyI/AAAAAAAAAEE/4jM8LZ5IXr0/-cole-tableau_clr.gif')");
  //positions.push("bottom right");
  
  // dynamics images
  for (var i = 0; i < numberOfBGElement; i++) {
    var elt = makeBGElement(images, params);
    urls.push("url('"+elt.content+"')");
    positions.push(elt.left + "px " + elt.top + "px");
  }
  
	$(htmlElement).css("background-image", urls.join(",")); 
  $(htmlElement).css("background-position", positions.join(",")); 
	$(htmlElement).css("background-repeat", "no-repeat");          
}
  
function makeBGElement(shapes, options){
  return {
     left: Math.floor(Math.random() * options.maxLeft) + options.minLeft, 
     top: Math.floor(Math.random() * options.maxTop) + options.minTop,
     content: shapes[Math.floor(Math.random() * shapes.length)],
     rotation: Math.floor(Math.random() * 360),
     opacity: Math.random() * 1.0
  };  
}

function stylishSearchBox() {

  $("input.gsc-search-button").hide();
  //$("td.gsc-input").append('<div id="delete"><span id="x">x</span></div>');
  $("td.gsc-search-button").append('<button class="gbqfb" name="btnG"><span class="gbqfi"></span></button>');
      /*$("#gsc-search-button2").click(function () {
          $("input.gsc-search-button").click();
      });*/
      
      // if text input field value is not empty show the "X" button
      /*$("input.gsc-input").keyup(function() {
        $("#x").fadeIn();
        if ($.trim($("input.gsc-input").val()) == "") {
          $("#x").fadeOut();
        }
      });*/
      // on click of "X", delete input field value and hide "X"
      /*$("#x").click(function() {
        $("input.gsc-input").val("");
        $(this).hide();
      });*/
}
